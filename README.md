Title:  pd-mdnsbrowser
Author: Albert Gräf <aggraef@gmail.com>
Date:   2014-09-30

pd-mdnsbrowser
==============

This is a stand-alone version of the OSC service browser in the pd-touchosc
module. It employs mDNS/DNS-SD a.k.a. *Zeroconf* for the service discovery,
and works with the prevalent Zeroconf implementations on Linux and Mac OS X,
Avahi and Bonjour.

The software is provided as an external for Miller Puckette's popular
graphical multimedia environment Pd a.k.a. PureData, so that you can interface
to Zeroconf services from Pd. In difference to the oscbrowser object included
in pd-touchosc, it is not restricted to the `_osc._udp` service type (although
this is probably its most common application in Pd), so that arbitrary
Zeroconf service types may be implemented. However, it's still limited to the
default Zeroconf domain (usually the local network) right now.

The mdnsbrowser object provides you with a way to both discover Zeroconf
services *and* have Pd provide such services, which makes setting up network
connections between the services much easier, since the user doesn't have to
figure out the actual network addresses. One obvious application are services
which transmit [OSC](http://opensoundcontrol.org/) (Open Sound Control)
messages from/to Pd. But this external may conceivably be used to implement
any kind of networked-based protocol between Pd and other host-based or mobile
applications. Note, however, that the external only provides the basic
machinery for publishing and discovering services, so the services themselves
still need to be implemented by other Pd externals and/or 3rd party
applications. In the case of OSC this would be the mrpeach externals on the Pd
side together with external OSC hardware and software such as the Lemur and
TouchOSC.

There aren't all that many Zeroconf implementations for Pd. To the author's
knowledge, the only other Pd object of this kind is Murray Foster's
[simplebonjour](https://github.com/bonemurmurer/simplebonjour) which doesn't
seem to be actively maintained any longer and is limited to service discovery
(the DNS-SD part) only.

License
=======

pd-mdnsbrowser is Copyright (c) 2014 by Albert Gräf. It is distributed under
the 3-clause BSD license, please check the COPYING file for details.

Installation
============

You need:

- [Pd](http://puredata.info). The included help patch also requires the
  mrpeach externals to make the OSC service example work. We recommend using
  one of the available compilations such as pd-extended or pd-l2ork which
  already includes the required OSC externals, but you can also use vanilla Pd
  if you install the mrpeach externals manually.

- [Pure](http://purelang.bitbucket.org/). pd-mdnsbrowser is written in the
  author's Pure programming language, so you need to have the Pure interpreter
  installed, as well as the pd-pure plugin loader. You can find these on the
  [Pure website](http://purelang.bitbucket.org/).

- For the Zeroconf implementation, you'll also need the pure-avahi or
  pure-bonjour module (pd-mdnsbrowser will work with either of these; usually
  you use pure-avahi on Linux and pure-bonjour on the Mac). These can also be
  found on the Pure website. Of course, you'll also need to have the Avahi or
  Bonjour service running on your local network. If you you have a Mac then
  this should usually be the case already, but on Linux you'll have to both
  install Avahi and enable the corresponding service so that it is started
  automatically at boot time.

Users of Arch Linux may want to check the Arch User Repositories (AUR), as we
try to maintain a fairly complete and recent collection of Pure- and
Pd-related packages there.

Mac OS X users can find a ready-made binary package for 64 bit Intel systems
here: [pd-mdnsbrowser-0.1-macosx-x86_64.zip](https://bitbucket.org/agraef/pd-mdnsbrowser/downloads/pd-mdnsbrowser-0.1-macosx-x86_64.zip). The
zip file contains the `mdnsbrowser` directory which you'll have to copy to a
directory on Pd's library path (usually `/Library/Pd` for system-wide and
`~/Library/Pd` for personal installation on the Mac). (You'll also need the
Pure interpreter and the pure-bonjour module which are available in
[MacPorts](http://www.macports.org/). Please check the [Pure On Mac OS
X](https://bitbucket.org/purelang/pure-lang/wiki/PureOnMacOSX) wiki page for
details.)

To compile the software yourself, check the included Makefile for settings
that might need to be adjusted for your system, then run:

    make
    sudo make install

This works with vanilla Pd. If you're running pd-extended or pd-l2ork then
during installation you need to specify the Pd flavor using the `PD` make
variable, e.g.:

    sudo make install PD=pd-extended

Help and Documentation
======================

A help patch is included which demonstrates the use of this external. Open the
Pd help browser and look for a section named `mdnsbrowser`. You should find a
patch named `mdnsbrowser-help` there.

Feedback and Bug Reports
========================

As usual, bug reports, patches, feature requests, other comments and source
contributions are more than welcome. Just drop me an email, file an issue at
the tracker or send me a pull request on pd-mdnsbrowser's Bitbucket page
<https://bitbucket.org/agraef/pd-mdnsbrowser>.

Enjoy! :)

Albert Gräf <aggraef@gmail.com>
